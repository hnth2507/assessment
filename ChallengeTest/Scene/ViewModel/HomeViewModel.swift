//
//  HomeViewModel.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit


let updateDataInterval = 60.0

protocol HomeViewModelProtocol {
    func didReceivePostData()
}

class HomeViewModel: NSObject {

    var postData: [Post]?
    var delegate: HomeViewModelProtocol?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    var updateTimer: Timer?
    
    init(delegate: HomeViewModelProtocol) {
        super.init()
        self.delegate = delegate
        self.setupOutPut()
    }
    
    func setupOutPut() {
        let cachedData = PostDatabase.shareInstance.getAllPost()
        if cachedData.count > 0 {
            print(cachedData)
            self.postData = cachedData 
            if let viewController = self.delegate {
                viewController.didReceivePostData()
            }
        } else {
            fetchFromserver()
        }
        self.syncData()
    }
    
    func fetchFromserver() {
        print("Fetch data from server")
        PostService.sharedInstance.getPost { (dict) in
            if let data = dict as? [String: Any?] {
                let list = PostList(data)
                let sortedList = list.data?.sorted { (post1, post2) -> Bool in
                    let date1 = Date.dateFrom(dateStr: post1.postedAt!)
                    let date2 = Date.dateFrom(dateStr: post2.postedAt!)
                    return date1?.compare(date2!) == ComparisonResult.orderedDescending
                }
                PostDatabase.shareInstance.savePosts(post: sortedList!)
                self.postData = sortedList
                if let viewController = self.delegate {
                    viewController.didReceivePostData()
                }
            }
        }
    }
}


extension HomeViewModel {
    func syncData() {
        updateTimer = Timer.scheduledTimer(timeInterval: updateDataInterval, target: self,
                                           selector: #selector(fetchFromserver), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
    }
    
    func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }

}


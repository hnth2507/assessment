//
//  PostItemCell.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/11/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit


class PostItemCell: UITableViewCell {

    @IBOutlet weak var avatarImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var photoImgView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var post: Post!
    var isRequestLike = false
    var isRequestComment = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customizeUI() {
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 6, width: self.frame.size.width, height: self.frame.height - 12))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        self.contentView.addSubview(whiteRoundedView)
        self.contentView.sendSubview(toBack: whiteRoundedView)
        
        self.avatarImgView.layer.masksToBounds = true
        self.avatarImgView.layer.cornerRadius = self.avatarImgView.frame.width/2
    }


    func popularData(model: Post?) {
        if let data = model {
            post = data
            if let user = post.user {
                self.nameLabel.text = user.fullName
                if let url = user.imageURL {
                    self.avatarImgView.loadImageUsingCacheWithUrl(urlString: url, completion: {})
                }
            }
            self.messageLabel.text = post.message
            self.timeLabel.text = Date.dateFrom(dateStr: post.postedAt!)?.getIntervalFromNow()
            self.indicator.startAnimating()
            self.photoImgView.loadImageUsingCacheWithUrl(urlString: post.imageURL!, completion: { self.indicator.stopAnimating() })
            self.updateLike()
            self.updateComment()
        }
        customizeUI()
    }
    
    func updateLike() {
        if let likes =  post?.likeCount {
            if likes < 0 {
                self.getLike()
            } else {
                self.likeCountLabel.text = likes > 1 ? ("\(likes) likes") : ("\(likes) like")
                self.setNeedsDisplay()
            }
        }
    }

    func updateComment() {
        if let comments =  post?.commentCount {
            if comments < 0 {
                getCommend()
            } else {
                self.commentCountLabel.text = comments > 1 ? ("\(comments) comments") : ("\(comments) comment")
                self.setNeedsDisplay()
            }
        }
    }
}


extension PostItemCell {
    
    func getLike() {
        if isRequestLike {return}
        if let id = post._id {
            isRequestLike = true
            PostService.sharedInstance.getLikeCount(postId: id, handler: { dict in
                self.isRequestLike = false
                if let data = dict as? [String: Any?] {
                    let response = Response(data)
                    if let likes = response.data as? Int {
                        self.post.likeCount = Int32(likes)
                        PostDatabase.shareInstance.updateLikeCount(postId: self.post._id!, count: Int32(likes))
                        self.updateLike()
                    }
                }
                
            })
        }
    }
    
    func getCommend() {
        if isRequestComment {return}
        if let id = post._id {
            isRequestComment = true
            PostService.sharedInstance.getCommentCount(postId: id, handler: { dict in
                self.isRequestComment = false
                if let data = dict as? [String: Any?] {
                    let response = Response(data)
                    if let comments = response.data as? Int {
                        self.post.commentCount = Int32(comments)
                        PostDatabase.shareInstance.updateCommendCount(postId: self.post._id!, count: Int32(comments))
                        self.updateComment()
                    }
                }
                
            })
        }
    }

}

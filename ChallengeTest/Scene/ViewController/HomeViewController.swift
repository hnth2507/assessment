//
//  HomeViewController.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var viewModel: HomeViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewModel = HomeViewModel(delegate: self)
        self.viewModel?.delegate = self
        //self.tableView.register(PostItemCell.self, forCellReuseIdentifier: "PostItemCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}


extension HomeViewController: HomeViewModelProtocol {
    func didReceivePostData() {
        self.tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.viewModel?.postData?.count {
            return count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "PostItemCell", for: indexPath) as? PostItemCell
        if cell == nil {
            cell = PostItemCell(style: UITableViewCellStyle.default, reuseIdentifier: "PostItemCell")

        }
        if let model = self.viewModel?.postData?[indexPath.row] {
            cell?.popularData(model: model)
        }
        return cell!
    }
    
   
}

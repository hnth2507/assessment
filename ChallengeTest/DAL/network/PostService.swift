//
//  PostService.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit

enum PostTarget {
    case getPosts()
    case getCommentCount(postId: Int32)
    case getLikeCount(postId: Int32)
}

extension PostTarget: TargetType {
    
    var baseURL: URL {
        let url = URL(string: "http://thedemoapp.herokuapp.com")
        return url!
    }
    
    var path: String {
        switch self {
        case .getPosts():
            return "/post"
        case .getLikeCount(let postId):
            return "/post/\(postId)/likeCount"
        case .getCommentCount(let postId):
            return "/post/\(postId)/commentCount"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getPosts():
            return .GET
        case .getLikeCount(_):
            return .GET
        case .getCommentCount(_):
            return .GET
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        default: return [:]
        }
    }
    
    var task: Task {
        switch self {
        default:
            return .request
        }
    }
}



class PostService: NSObject {

    static let sharedInstance = PostService()
    
    let provider = Provider<PostTarget>()
    override init() {
        super.init()
    }
    
    func getPost(_ handler: @escaping Completion) {
        provider.requestWith(target: .getPosts(), completion: handler)
    }
    
    func getCommentCount(postId: Int32, handler: @escaping Completion) {
        provider.requestWith(target: .getCommentCount(postId: postId), completion: handler)
    }
    
    func getLikeCount(postId: Int32, handler: @escaping Completion) {
        provider.requestWith(target: .getLikeCount(postId: postId), completion: handler)
    }
    
}

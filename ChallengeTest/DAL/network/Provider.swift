//
//  BaseService.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit
import CFNetwork

public enum HTTPMethod: String {
    case POST
    case GET
    case PUT
    case PATCH
    case DELETE
}

public enum Task {
    
    /// A basic request task.
    case request
    case download
    case upload
}

public protocol TargetType {
    
    /// The target's base `URL`.
    var baseURL: URL { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: HTTPMethod { get }
    
    /// The parameters to be incoded in the request.
    var parameters: [String: Any]? { get }
    
    
    /// The type of HTTP task to be performed.
    var task: Task { get }
    
}


typealias Completion = (_ response: NSDictionary) -> Void

open class Provider<Target: TargetType> {
    var hostURL: String?
    
    
    func requestWith(target: Target , completion: @escaping Completion ) {
        
        // Excute HTTP Request
        guard let request = self.requestFrom(target: target) else { return }
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            // Check for error
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Print out dictionary
                    print(convertedJsonIntoDict)
                    // Get value by key
                    DispatchQueue.main.async {
                        completion(convertedJsonIntoDict)
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
    
    func requestFrom(target: Target) -> URLRequest? {
        
        let requestURL = target.baseURL.appendingPathComponent(target.path)
        var request = URLRequest(url: requestURL);
        request.httpMethod = target.method.rawValue
        request.setValue("application/json",
                            forHTTPHeaderField: "Content-Type")
        request.setValue("application/json",
                            forHTTPHeaderField: "Accept")

        
        
        return request
    }
}

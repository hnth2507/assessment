//
//  PostDatabase.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit
import CoreData

class PostDatabase: DataManager {

    static let shareInstance = PostDatabase()
    var post: PostModel?
    
    override init() {
        super.init()
    }
    
    func getAllPost() -> [Post] {
        var posts: [Post] = []
        do {
            let request = NSFetchRequest<PostModel>(entityName: "PostModel")
            let sectionSortDescriptor = NSSortDescriptor(key: "postedAt", ascending: false)
            let sortDescriptors = [sectionSortDescriptor]
            request.sortDescriptors = sortDescriptors
            let data: [PostModel] = try databaseContext!.fetch(request)
            if data.count > 0 {
                posts = data.map { object -> Post in
                    var post = Post()
                    post._id = object.postId
                    post.imageURL = object.imageURL
                    post.message = object.message
                    post.postedAt = Date.toString(date: (object.postedAt as Date?)!)
                    print("post at", post.postedAt ?? "")
                    post.likeCount = object.likes
                    post.commentCount = object.comments
                    post.user = User()
                    post.user?._id = object.userId
                    post.user?.fullName = object.userFullName
                    post.user?.dob = object.userDob
                    post.user?.imageURL = object.userImageURL
                    post.user?.mobileNo = object.userMobileNo
                    return post
                }
            }
        } catch {
            print("Fetching Failed")
        }
        
        return posts
    }
    
    func savePosts(post: [Post]) {
        
        post.forEach { post in
            if let object = self.getPostwith(postId: post._id!) {
                object.message = post.message
                object.postedAt = Date.dateFrom(dateStr: post.postedAt!)! as NSDate
                object.imageURL = post.imageURL
                object.likes = post.likeCount
                object.comments = post.commentCount
                object.userId = (post.user?._id)!
                object.userImageURL = post.user?.imageURL
                object.userFullName = post.user?.fullName
                object.userDob = post.user?.dob
                object.userMobileNo = post.user?.mobileNo
            } else {
                let entity = NSEntityDescription.entity(forEntityName: "PostModel",in: self.databaseContext!)!
                let model = PostModel(entity: entity, insertInto: databaseContext)
                model.postId = post._id!
                model.message = post.message
                model.postedAt = Date.dateFrom(dateStr: post.postedAt!)! as NSDate
                model.imageURL = post.imageURL
                model.likes = post.likeCount
                model.comments = post.commentCount
                model.userId = (post.user?._id)!
                model.userImageURL = post.user?.imageURL
                model.userFullName = post.user?.fullName
                model.userDob = post.user?.dob
                model.userMobileNo = post.user?.mobileNo
            }
        }
        self.saveContext()
    }
    
    func updateLikeCount(postId: Int32, count: Int32) {
        let request = NSFetchRequest<PostModel>(entityName: "PostModel")
        request.predicate = NSPredicate(format:"postId == %d", postId)
        do {
            let result: [PostModel] = try databaseContext!.fetch(request)
            if let post = result.first {
                post.likes = count
            }
        } catch {
            print("Update likes Failed")
        }
        
        self.saveContext()
        
    }
    
    
    func updateCommendCount(postId: Int32, count: Int32) {
        let request = NSFetchRequest<PostModel>(entityName: "PostModel")
        request.predicate = NSPredicate(format:"postId == %d", postId)
        do {
            let result: [PostModel] = try databaseContext!.fetch(request)
            if let post = result.first {
                post.comments = count
            }
        } catch {
            print("Update commend Failed")
        }
        
        self.saveContext()
        
    }
    
    func getPostwith(postId: Int32) -> PostModel? {
        let request = NSFetchRequest<PostModel>(entityName: "PostModel")
        request.predicate = NSPredicate(format:"postId == %d", postId)
        do {
            let result: [PostModel] = try databaseContext!.fetch(request)
            if result.count > 0 { return result.first }
            return nil
        } catch {
            print("Update likes Failed")
        }
        return nil

    }
}

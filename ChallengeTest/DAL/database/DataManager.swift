//
//  DataManager.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import UIKit
import CoreData

class DataManager: NSObject {

    var databaseContext: NSManagedObjectContext? {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return nil
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        return managedContext
    }
    
    override init() {
        super.init()
    }
    
    func saveContext() {
        if let context = databaseContext {
            if context.hasChanges {
                do {
                    try context.save()
                } catch let error as NSError {
                    print("Ops there was an error \(error.localizedDescription)")
                    abort()
                }
            }

        }
    }
    
}

//
//  Models.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import Foundation


struct User {
    var _id: Int32?
    var fullName: String?
    var mobileNo: String?
    var dob: String?
    var imageURL: String?
    
    init() {
    }
    
    init(_ dict: [String: Any?]) {
        self._id = dict["_id"] as? Int32
        self.fullName = dict["fullName"] as? String
        self.mobileNo = dict["mobileNo"] as? String
        self.dob = dict["doc"] as? String
        self.imageURL = dict["imageURL"] as? String
    }
}

struct Post {
    var _id: Int32?
    var user: User?
    var message: String?
    var postedAt: String?
    var imageURL: String?
    var likeCount: Int32 = -1
    var commentCount: Int32 = -1
    
    init() {
        
    }
    
    init(_ dict: [String: Any?]) {
        self._id = dict["_id"] as? Int32
        self.user = User(dict["user"] as! [String : Any?])
        self.message = dict["message"] as? String
        self.postedAt = dict["postedAt"] as? String
        self.imageURL = dict["imageURL"] as? String
    }

}

struct PostList {
    var code: Int32?
    var status: String?
    var data: [Post]?
    
    init(_ dict: [String: Any?]) {
        self.code = dict["code"] as? Int32
        self.status = dict["status"] as? String
        if let posts = dict["data"] as? [[String: Any]] {
            self.data = posts.map { post in return Post(post)  }
        }
    }

}

struct Response {
    var code: Int32?
    var status: String?
    var data: Any?
    init(_ dict: [String: Any?]) {
        self.code = dict["code"] as? Int32
        self.status = dict["status"] as? String
        self.data = dict["data"] ?? 0
    }
}


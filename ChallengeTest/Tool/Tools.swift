//
//  Tools.swift
//  ChallengeTest
//
//  Created by Thi Huynh on 5/13/17.
//  Copyright © 2017 Nexx. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

public extension UIImageView {
    func loadImageUsingCacheWithUrl(urlString: String, completion: @escaping (Void)-> Void) {
        self.image = nil
        
        // check for cache
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        // download image from url
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                return
            }
            if let image = UIImage(data: data!) {
                DispatchQueue.main.async(execute: { () -> Void in
                    imageCache.setObject(image, forKey: urlString as AnyObject)
                    self.image = image
                    self.alpha = 0.0
                    UIView.animate(withDuration: 0.3) { self.alpha = 1.0 }
                    completion()
                })
            }
        }).resume()
    }
}


extension Date {
    
    static func toString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone.local
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    static func dateFrom(dateStr: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone.local
        
        guard let date = dateFormatter.date(from: dateStr) else {
            assert(false, "no date from string")
            return nil
        }
        return date
    }
    
    func getIntervalFromNow() -> String? {
        let now = Date()
        let calendar = NSCalendar.current
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: self, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            return "\(year) years ago"
        }
        
        if let year = components.year, year >= 1 {
            return "Last year"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) months ago"
        }
        
        if let month = components.month, month >= 1 {
            return "Last month"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week) weeks ago"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "Last week"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day) days ago"
        }
        
        if let day = components.day, day >= 1 {
            return "Yesterday"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour) hours ago"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "An hour ago"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute) minutes ago"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "A minute ago"
        }
        
        if let second = components.second, second >= 3 {
            return "\(second) seconds ago"
        }
        
        return "Just now"
        
    }
}

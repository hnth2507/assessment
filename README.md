# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : the assessment for application of iOS developer in User Experience
* Version 1.0

### How do I get set up? ###

* Xcode: 8.3
* Swift: 3.0
* Structure
  - MVVM : Model View View Model (View Controller, View Model and Model).
  - DAL  : data from web-service (Post Service) and data from local database (PostDatabase) (Core Data and SQLite)
  - Provider: URL request wrapper and follow the design like Moya, use target for each end point
  - Tools: All helper functions use in project
  - Model: Use struct to store model data.
* Dependencies : no
* Database configuration: no
* Deployment instructions: Build (cmd + B) first to generate the NSManageObject and run (cmd + R)